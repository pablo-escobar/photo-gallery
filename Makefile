.PHONY: build deploy

build:
	# TZ=-0000 is used to hide my local timezone from lazygal
	env TZ=-0000 lazygal photos/ \
					             -o _site/\
					             -t "$$(pwd)/theme" \
					             -T 150x150 \
					             -s default=800x800 \
											 --check-all-dirs \
					             --pic-sort-by=numeric
	cp 404.html _site/

deploy: build
	rsync -rtv _site/ root@pablo.escobar.life:/var/www/photos

runserver:
	cd _site && python3 -m http.server
